var promptly = require('promptly');
var waterfall = require('async/waterfall');
var db = require('./server/libs/database');

waterfall([
    function database(done){
        promptly.prompt('Database connection: ', { default: 'postgres://postgres:password@localhost:5432' }, done);
    },
    function createDatabase(arg, done){
        db.run(arg, done);
    }],
    function (err, results) {

        if (err) {
            console.error('Setup failed.');
            console.error(err);
            return process.exit(1);
        }

        console.log('Setup complete.');
        process.exit(0);
    });
