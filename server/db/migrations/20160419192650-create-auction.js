'use strict';
module.exports = {
    up: function(queryInterface, Sequelize) {
        return queryInterface.createTable('Auctions', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            quantity: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            minimunBid: {
                allowNull: false,
                type: Sequelize.DECIMAL(10, 2)
            },
            duration: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            winningBid: {
                allowNull: true,
                type: Sequelize.DECIMAL(10, 2)
            },
            startedAt: {
                allowNull: true,
                type: Sequelize.DATE
            },
            completedAt: {
                allowNull: true,
                type: Sequelize.DATE
            },
            status: {
                allowNull: false,
                type: Sequelize.ENUM('pending', 'active', 'completed'),
                defaultValue: 'pending'
            },
            userId: {
                allowNull: false,
                type: Sequelize.INTEGER,
                references: {
                    model: 'Users',
                    key: 'id'
                },
            },
            itemId: {
                allowNull: false,
                type: Sequelize.INTEGER,
                references: {
                    model: 'Items',
                    key: 'id'
                }
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    down: function(queryInterface, Sequelize) {
        return queryInterface.dropTable('Auctions');
    }
};
