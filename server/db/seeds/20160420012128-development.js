'use strict';

module.exports = {
    up: function (queryInterface, Sequelize) {

        return queryInterface.bulkInsert('Items', [{
            name: 'Bread',
            image: 'bread.png',
            createdAt: new Date(),
            updatedAt: new Date()
        },
        {
            name: 'Carrot',
            image: 'carrot.png',
            createdAt: new Date(),
            updatedAt: new Date()
        },
        {
            name: 'Diamond',
            image: 'diamond.png',
            createdAt: new Date(),
            updatedAt: new Date()
        }], {});
    },

down: function (queryInterface, Sequelize) {

        return queryInterface.bulkDelete('Items', null, {});
    }
};
