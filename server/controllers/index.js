'use strict';

var express = require('express'),
    router = express.Router();

router.use('/sessions', require('controllers/sessions'));
router.use('/users', require('controllers/users'));
router.use('/auctions', require('controllers/auctions'));
// router.use('/auctions', require('controllers/bids'));

module.exports = router;
