'use strict';

var express = require('express'),
    validate = require('express-validation'),
    router = express.Router(),
    commands = require('commands'),
    validator = require('validations').sessionCreate,
    jwt = require('jsonwebtoken'),
    config = require('config/app');


router.post('/', validate(validator), function(req, res) {
    commands.createUser(req.body, function(err, user) {
        if (err) {
            console.error(err);
            return res.status(403).send('Access Denied.');
        }

        var token = jwt.sign({
            id: user.id,
            name: user.name
        }, config.secretKey, {
            expiresIn: '24h'
        });

        res.status(201).json({token: token});
    });
});

module.exports = router;
