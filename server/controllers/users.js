'use strict';

var express = require('express'),
    validate = require('express-validation'),
    merge = require('lodash/merge'),
    router = express.Router(),
    authenticate = require('middlewares/authHandler'),
    queries = require('queries').users;


router.get('/:id', authenticate, function(req, res) {
    queries.findById(req.params.id, function(err, user) {
        if (err) {
            console.error(err);
            return res.sendStatus(404);
        }

        res.status(200).json(user);
    });
});

module.exports = router;
