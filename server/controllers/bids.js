'use strict';

var express = require('express'),
    validate = require('express-validation'),
    merge = require('lodash/merge'),
    router = express.Router(),
    commands = require('commands'),
    validator = require('validations').placeBid,
    authenticate = require('middlewares/authHandler');


router.post('/:id/bids', authenticate, validate(validator), function(req, res) {
    var attrs = merge({ auctionId: req.params.id }, req.body);

    commands.placeBid(attrs, function(err) {
        if (err) {
            console.error(err);
            return res.sendStatus(400);
        }

        res.sendStatus(201);
    });
});

module.exports = router;
