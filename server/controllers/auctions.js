'use strict';

var express = require('express'),
    validate = require('express-validation'),
    router = express.Router(),
    commands = require('commands'),
    validator = require('validations').auctionCreate,
    authenticate = require('middlewares/authHandler');


router.post('/', authenticate, validate(validator), function(req, res) {
    commands.createAuction(req.body, function(err, auction) {
        if (err) {
            console.error(err);
            return res.sendStatus(400);
        }

        res.sendStatus(201);
    });
});

module.exports = router;
