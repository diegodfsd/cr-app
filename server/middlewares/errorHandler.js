'use strict';

var ev = require('express-validation');

module.exports = function(err, req, res, next) {
    // in case the response started to be sent
    if (res.headersSent) {
        return next(err);
    }

    // specific for validation errors
    if (err instanceof ev.ValidationError) {
        return res.status(err.status).json(err);
    }

    res.status(err.status || 500).send({ message: err.message, stack: err.stack });
};
