'use strict';

var jwt = require('jsonwebtoken'),
    config = require('config/app');

module.exports = function(req, res, next) {
    var token = req.headers['authorization'] || req.headers['x-access-token'] || req.body.token || req.query.token;
    token = token.replace(/bearer\s/i, '');

    if (token) {
        jwt.verify(token, config.secretKey, function(err, decoded) {
            if (err) {
                console.log(err);
                return res.status(401).json({ message: 'Unauthorized.' });
            } else {
                req.identity = decoded;
                next();
            }
        });
    } else {
        return res.status(401).send({ message: 'Unauthorized.' });
    }
};
