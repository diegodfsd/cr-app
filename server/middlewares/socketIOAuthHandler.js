'use strict';

var jwt = require('jsonwebtoken'),
    config = require('config/app');

module.exports = function(io) {
    io.on('connection', function(socket) {
        //temporarily delete socket from namespace connected map
        delete io.sockets.connected[socket.id];

        var timer = setTimeout(function () {
            socket.disconnect('unauthorized');
        }, 5000);

        var authenticate = function (data) {
            clearTimeout(timer);

            jwt.verify(data.token, config.secret, function(err, decoded) {
                if (err) {
                    socket.disconnect('unauthorized');
                }

                if (!err && decoded) {
                    //restore the temporarily disabled connection
                    console.log(decoded);
                    socket.username = decoded.name;
                    socket.connectedAt = new Date();

                    io.sockets.connected[socket.id] = socket;

                    // disconnect listener
                    socket.on('disconnect', function () {
                        console.info('SOCKET [%s] DISCONNECTED', socket.id);
                    });

                    console.info('SOCKET [%s] CONNECTED', socket.id);
                    socket.emit('authenticated');
                }
            });
        };

        socket.on('authenticate', authenticate );
    });
};
