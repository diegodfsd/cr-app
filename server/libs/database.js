'use strict';

var Sequelize = require('sequelize');
var pg = require('pg');
var env = process.env.NODE_ENV || 'development';
var settings = require('./../config/database')[env];

exports.run = function run(arg, done) {
    var conString = [arg, 'postgres'].join('/');

    pg.connect(conString, function(err, client) {
        if (err){
            return done(err);
        }
        // create the db and ignore any errors, for example if it already exists.
        var query = 'CREATE DATABASE ' + settings.database;

        client.query(query, function(err) {
            client.end();
            done();
        });
    });
};
