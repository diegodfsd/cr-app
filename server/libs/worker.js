'use strict';

var EventEmitter = require('events'),
    util = require('util');

/**
 * Worker to work like a timer
 * @param {Number} interval - An interval in mileseconds
 */
function Worker(interval) {
    EventEmitter.call(this);

    this.interval = (!interval || interval <= 0)? 1000 : interval;
    this.timer = null;
}

util.inherits(Worker, EventEmitter);

/**
 * Start worker and emit a tick event
 * @return {Boolean} A flag to indicate success
 */
Worker.prototype.start = function() {
    if (this.started()) { return false; }

    var self = this;

    self.emit('start');

    self.timer = setTimeout(function(){
        self.emit('tick');
    }, self.interval);

    return true;
};

/**
 * Stop worker and emit a end event
 */
Worker.prototype.stop = function() {
    clearTimeout(this.timer);
    this.timer = null;
    this.emit('end');
};

/**
 * Start worker and emit a tick
 * @return {Boolean} A flag to indicate whether it is started or it is not
 */
Worker.prototype.started = function() {
  return !!this.timer;
};

module.exports = Worker;
