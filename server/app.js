'use strict';

// The next two lines allow using require with absolute path without using ./
process.env.NODE_PATH = __dirname;
require('module').Module._initPaths();

var http = require('http'),
    express = require('express'),
    app = express(),
    socketioAuth = require('socketio-auth'),
    bodyParser = require('body-parser'),
    morgan = require('morgan'),
    notfoundHandler = require('middlewares/notfoundHandler');


app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(require('middlewares/errorHandler'));
app.use('/api/*', function(req, res, next) {
    notfoundHandler.send404(req, res);
});
app.use('/api', require('controllers'));


app.use(express.static('./client/'));
app.use(express.static('./'));
// Any invalid calls for templateUrls are under api/* and should return 404
app.use('/api/*', function(req, res, next) {
    notfoundHandler.send404(req, res);
});

// Any deep link calls should return index.html
app.use('/*', express.static('./client/index.html'));

module.exports = app;


/*
Estou pensando em me autenticar usando socket.io, no sucesso da autenticacao retorno os dados do usuário via socket.io
no angular eu terei evento para atualizar dados do usuario, para atualizar o leilao atual, timer e para criar leilao e
dar um lance eu uso api rest.

==ideias
1. eu posso tratar os eventos do socket.io como controllers a titulo de organizacao, por exemplo eu poderia ter
inves de ter um middleware de authenticacao eu teria um controller sessions com os mesmos metodos

2. na index dos controllers posso receber parametros como por exemplo, io e pra isso todo controller teria de
ser uma factory
*/
