'use strict';

var Joi = require('joi');

module.exports = {
    sessionCreate: {
        body: {
            name: Joi.string().required()
        }
    },
    auctionCreate: {
        body: {
            userId: Joi.number().integer().required(),
            itemId: Joi.number().integer().required(),
            quantity: Joi.number().integer().required(),
            minimunbid: Joi.number().precision(2).required()
        }
    },
    placeBid: {
        params: {
            id: Joi.number().integer().required()
        },
        body: {
            userId: Joi.number().integer().required(),
            value: Joi.number().precision(2).required()
        }
    }
};
