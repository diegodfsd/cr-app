'use strict';

var models = require('models'),
    User = models.User,
    Item = models.Item,
    Inventory = models.Inventory;

module.exports = ((function() {

    function findByName(name, done) {
        User.findOne({
            include: [{ model: Inventory }],
            where: { name: name }
        }).then(function(result) {
            done(null, result);
        }).catch(function(err) {
            done(err);
        });
    }

    function findById(id, done) {
        User.findOne({
            include: [{
                model: Item,
                as: 'Items',
                through: {
                    attributes: ['amount']
                }
            }],
            where: { id: id }
        }).then(function(result) {
            done(null, result);
        }).catch(function(err) {
            done(err);
        });
    }


    return {
        findByName: findByName,
        findById: findById
    };

})());
