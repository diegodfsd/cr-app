'use strict';

var models = require('models'),
    Auction = models.Auction,
    Bid = models.Bid;

module.exports = ((function() {

    function findByUserId(userId, done) {
        Auction.findAll({
            include: [{ model: Bid }],
            where: { userId: userId, status: { $ne: 'completed' } }
        }).then(function(result) {
            done(null, result);
        }).catch(function(err) {
            done(err);
        });
    }

    return {
        findByUserId: findByUserId
    };

})());
