'use strict';

var path = require('path'),
    fs = require('fs'),
    lodash = require('lodash/object');

module.exports = (function () {
    var queries = {};

    fs.readdirSync(__dirname).forEach(function (file) {
        if (file === 'index.js') {
            return;
        }

        var query = require(path.join(__dirname, file));
        queries[path.basename(file, '.js')] = query;
    });

    return queries;
})();
