'use strict';

var promise = require('bluebird'),
    models = require('models'),
    User = models.User,
    Item = models.Item,
    Auction = models.Auction,
    Inventory = models.Inventory,
    lodash = require('lodash/collection');

function validate (attrs, next) {
    promise.all([Auction.findAll({
        where: { userId: attrs.userId,
            itemId: attrs.itemId,
            status: { $ne: 'completed' }
        }}),
        User.findOne({
            where: { id: attrs.userId },
            include: [{ model: Inventory }]
        })])
        .spread(function(auctions, user) {

            var total = lodash.reduce(auctions, function(sum, curr) {
                return sum + curr.quantity;
            }, 0);

            var item = lodash.find(user.items, { id: attrs.itemId });

            var exceeded = (total + attrs.quantity) >= item.amount;

            next(null, exceeded);
        }).catch(function(err) {
            next(err);
        });
    }


    function process (attrs, done) {
        promise.all([
            User.findById(attrs.userId),
            Item.findById(attrs.itemId)
        ]).spread(function(user, item) {

            return Auction.create({
                userId: user.id,
                itemId: item.id,
                quantity: attrs.quantity,
                minimunbid: attrs.minimunbid,
                duration: 90,
                status: 'pending'
            });
        }).then(function(auction){
            done(null, auction);
        }).catch(function(err) {
            done(err);
        });
    }

    exports.handle = function (attrs, done) {
        validate(attrs, function(err, exceeded) {
            if (err) {
                return done(err);
            }

            if (exceeded) {
                return done(new Error('Unavailable quantity of the item.'));
            }

            process(attrs, done);
        });
    };
