'use strict';

var promise = require('bluebird'),
    Bid = require('models').Bid,
    Auction = require('models').Auction;

exports.handle = function (attrs, done) {
    Auction.findById(attrs.auctionId)
    .then(function(auction) {
        var bid = {
            userId: attrs.userId,
            auctionId: auction.id,
            value: attrs.value
        };

        return auction.placeBid(bid);
    }).then(function(bid) {
        done(null, bid);
    }).catch(function(err) {
        done(err);
    });
};
