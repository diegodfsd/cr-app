'use strict';

var path = require('path'),
    fs = require('fs');

module.exports = (function () {
    var commands = {};

    fs.readdirSync(__dirname).forEach(function(file) {
        if (file === 'index.js') {
            return;
        }

        var command = require(path.join(__dirname, file));
        commands[path.basename(file, '.js')] = command.handle;
    });

    return commands;
})();
