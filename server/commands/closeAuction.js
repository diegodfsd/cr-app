'use strict';

var promise = require('bluebird'),
    async = require('async'),
    models = require('models'),
    User = models.User,
    Item = models.Item,
    Auction = models.Auction,
    Bid = models.Bid,
    Inventory = models.Inventory;

exports.handle = function(attrs, done) {

    async.auto({
        findAuction: function(next) {
            Auction.findOne({
                where: { id: attrs.id },
                include: [{ model: User }, { model: Item }]
            }).then(next);
        },
        findSellerInventory: function(results, next) {
            var auction = results[0];

            Inventory.findOne({
                where: { userId: auction.User.id, itemId: auction.Item.id }
            }).then(next);
        },
        findBid: function(results, next) {
            var auction = results[0];

            Bid.findOne({
                where: { auctionId: auction.id },
                include: [{ model: User }]
            }).then(next);
        },
        findBuyerInventory: function(results, next) {
            var auction = results[0];
            var bid = results[2];

            Inventory.findOne({
                where: { userId: bid.User.id, itemId: auction.Item.id }
            }).then(next);
        },
        updateAmount: function(results, next) {
            var auction = results[0];
            var sellerInventory = results[1];
            var buyerInventory = results[1];

            sellerInventory.decreaseAmount(auction.quantity);
            buyerInventory.increaseAmount(auction.quantity);

            promise.all([sellerInventory.save, buyerInventory.save]).then(next);
        },
        pay: function(results, next) {
            var auction = results[0];
            var bid = results[2];
            var seller = auction.User;
            var buyer = bid.User;

            seller.credit(bid.value);
            buyer.debit(bid.value);

            promise.all([seller.save, buyer.save]).then(next);
        },
        closeAuction: function(results, next){
            var auction = results[0];
            auction.update({ status: 'completed' }).then(next);
        }
    },
    function(err, results) {
        if (err) {
            console.log(err);
            return done(err);
        }

        done(null);
    });
};
