'use strict';

var promise = require('bluebird'),
    async = require('async'),
    models = require('models'),
    Auction = models.Auction;

exports.handle = function(attrs, done) {
    Auction.findOne({
        where: { id: attrs.id }
    }).then(function(auction) {
        return auction.update({ status: 'Active' });
    }).then(function() {
        done(null);
    }).catch(function(err) {
        done(err);
    });
};
