'use strict';

var promise = require('bluebird'),
    User = require('models').User,
    Item = require('models').Item;

exports.handle = function handle(attrs, done) {
    promise.all([
        User.findOrCreate({ where: { name: attrs.name, balance: 1000 }, defaults: attrs }),
        Item.findAll({ order: 'name asc'})
    ]).spread(function(results, items) {
        var user = results[0],
        created = results[1];

        if (created) {
            return promise.all([
                user.addItem(items[0], { amount: 30 }), //bread
                user.addItem(items[1], { amount: 18 }), //carrot
                user.addItem(items[2], { amount: 1 }) //diamond;
            ]);
        } else {
            return promise.resolve({});
        }
    })
    .then(function() {

        return User.findOne({
            where: { name: attrs.name }
        });
    }).then(function(user) {
        done(null, user);
    })
    .catch(function(err) {
        done(err);
    });
};
