'use strict';

module.exports = function(sequelize, DataTypes) {
    var Item = sequelize.define('Item', {
        name: {
            type: DataTypes.STRING(64),
            allowNull: false,
            validate: {
                notEmpty: true
            }
        },
        image: {
            type: DataTypes.STRING(128),
            allowNull: false,
            validate: {
                notEmpty: true
            }
        }
    }, {
        classMethods: {
            associate: function(models) {
                //Item.belongsToMany(models.Inventory, { as: 'inventories', through: 'Inventory', foreignKey: 'id', otherKey: 'itemId'});
            }
        }
    });
    return Item;
};
