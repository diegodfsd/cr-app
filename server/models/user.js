'use strict';

module.exports = function(sequelize, DataTypes) {
    var User = sequelize.define('User', {
        name: {
            type: DataTypes.STRING(64),
            allowNull: false,
            validate: {
                notEmpty: true
            }
        },
        balance: {
            type: DataTypes.DECIMAL(10, 2),
            allowNull: false,
            validate: {
                isDecimal: true
            }
        }
    }, {
        classMethods: {
            associate: function(models) {
                User.belongsToMany(models.Item, { as: 'Items', through: 'Inventory', foreignKey: 'userId', otherKey: 'itemId' });
            }
        },
        instanceMethods: {
            debit: function(value) {
                if (value <= this.balance ) {
                    this.balance -= value;
                } else {
                    throw Error('Insufficient founds.');
                }
            },
            credit: function (value) {
                this.balance += value;
            }
        }
    });
    return User;
};
