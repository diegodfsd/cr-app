'use strict';

var async = require('async'),
    promise = require('bluebird');

module.exports = function(sequelize, DataTypes) {
    var Auction = sequelize.define('Auction', {
        quantity: {
            type: DataTypes.INTEGER,
            allowNull: false,
            validate: {
                isInt: true
            }
        },
        minimunBid: {
            type: DataTypes.DECIMAL(10, 2),
            allowNull: false,
            validate: {
                isDecimal: true
            }
        },
        duration: {
            type: DataTypes.INTEGER,
            allowNull: false,
            validate: {
                isInt: true
            }
        },
        startedAt: {
            type: DataTypes.DATE,
            allowNull: true,
            validate: {
                isDate: true
            }
        },
        completedAt: {
            type: DataTypes.DATE,
            allowNull: true,
            validate: {
                isDate: true
            }
        },
        status: {
            type: DataTypes.ENUM('pending', 'active', 'completed'),
            allowNull: false,
            defaultValue: 'pending'
        },
        winningBid: {
            type: DataTypes.DECIMAL(10, 2),
            allowNull: true,
            validate: {
                isDecimal: true
            }
        }
    }, {
        classMethods: {
            associate: function(models) {
                Auction.belongsTo(models.User, { foreignKey: 'userId' });
                Auction.belongsTo(models.Item, { foreignKey: 'itemId' });
                Auction.hasMany(models.Bid, { foreignKey: 'auctionId' });
            }
        },
        instanceMethods: {
            placeBid: function (bid) {
                if (bid.value > (this.winningBid || 0)) {
                    this.update({ winningBid: bid.value });
                }

                return this.createBid(bid);
            }
        }
    });
    return Auction;
};
