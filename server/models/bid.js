'use strict';

module.exports = function(sequelize, DataTypes) {
    var Bid = sequelize.define('Bid', {
        value: {
            type: DataTypes.DECIMAL(10, 2),
            allowNull: false,
            validate: {
                isDecimal: true
            }
        }
    }, {
        classMethods: {
            associate: function(models) {
                Bid.belongsTo(models.User, { foreignKey: 'userId' });
                Bid.belongsTo(models.Auction, { foreignKey: 'auctionId' });
            }
        }
    });
    return Bid;
};
