'use strict';

module.exports = function(sequelize, DataTypes) {
    var Inventory = sequelize.define('Inventory', {
        amount: {
            type: DataTypes.INTEGER,
            allowNull: false,
            validate: {
                isInt: true
            }
        }
    }, {
        classMethods: {
            associate: function(models) {
                // associations can be defined here
            }
        },
        instanceMethods: {
            decreaseAmount: function (value) {
                if (value <= this.amount ) {
                    this.amount -= value;
                } else {
                    throw Error('Insufficient inventory.');
                }
            },
            increaseAmount: function (value) {
                this.amount -= value;
            }
        }
    });
    return Inventory;
};
