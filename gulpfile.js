'use strict';

var gulp = require('gulp-help')(require('gulp')),
    browserSync = require('browser-sync'),
    reload = browserSync.reload,
    jshint = require('gulp-jshint'),
    nodemon = require('gulp-nodemon'),
    taskListing = require('gulp-task-listing'),
    paths = require('./gulp.config');


var port = process.env.PORT || 7203;

/**
 * Gulp Tasks
 */

 gulp.task('help', taskListing);
 gulp.task('default', ['help']);

gulp.task('lint', function() {
  return gulp.src(paths.clientAppScripts)
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'));
});

gulp.task('nodemon', function (cb) {
    var called = false;

    return nodemon({
        script: paths.server,
        ext: 'js',
        ignore: ['node_modules'],
        env: {
            'PORT': port,
            'NODE_ENV': 'development'
        },
        watch: [paths.serverAppDirectory]
    })
    .on('start', function () {
        if (!called) {
            called = true;
            cb();
        }
    })
    .on('restart', function () {
        setTimeout(function () {
            reload({ stream: false });
        }, 1000);
    });
});


gulp.task('browser-sync', ['nodemon'], function () {
    browserSync({
        proxy: 'localhost:' + port,
        port: 3000,
        files: [
            paths.clientAppPath + '/**/*.*',
            '!' + paths.clientAppPath + '/bower_components'
        ],
        ghostMode: { // these are the defaults t,f,t,t
            clicks: true,
            location: false,
            forms: true,
            scroll: true
        },
        logLevel: 'debug',
        logPrefix: 'gulp-prefix',
        logFileChanges: true,
        notify: true,
        reloadDelay: 0,
        injectChanges: true,
        minify: false
    }, function(err){
        console.log(err);
    });
});

gulp.task('watch', function() {
  gulp.watch(paths.clientAppScripts, ['lint']);
});

gulp.task('serve', ['browser-sync', 'lint'], function(){
});
