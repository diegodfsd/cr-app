'use strict';

/**
 * @ngdoc overview
 * @name app
 * @description
 * # app
 *
 * Main module of the application.
 */
angular
  .module('app', [
      'ngMessages',
      'ngResource',
      'ngRoute',
      'ngSanitize',
      'ngAnimate'
  ])
  .config(['$routeProvider', function ($routeProvider) {

    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        authenticate: true
      })
      .when('/register', {
        templateUrl: 'views/register.html',
        controller: 'RegisterCtrl',
        authenticate: false,
        resolve: {
          auth: ['$location', '$rootScope', function ($location, $rootScope) {
            if ($rootScope.authenticated) {
              event.preventDefault();
              $location.path('/').replace();
            }
          }]
        }
      })
      .otherwise({
        redirectTo: '/'
      });

  }])
  .run(['$rootScope', '$location', function($rootScope, $location) {

    $rootScope.$on('$routeChangeStart', function(event, next) {
      // redirect to login page if not logged in
      var authenticated = $rootScope.authenticated;
      if (next.authenticate && !authenticated) {
          event.preventDefault();
          $rootScope.authenticated = false;
          $location.path('/register').replace();
      }
    });
  }]);
