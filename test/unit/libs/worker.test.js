'use strict';

var expect = require('expect.js'),
    Timer = require('../../../server/libs/worker');

describe('Worker', function() {
    var instance = null;

    beforeEach(function() {
        instance = new Timer(100);
    });

    describe('events', function() {
        describe('tick and start', function() {
            beforeEach(function() {
                instance.start();
            });

            afterEach(function() {
                instance.stop();
            });

            it('should emit tick event based on interval', function(done) {
                instance.on('tick', function() {
                    done();
                });
            });
        });
    });

    describe('#constructor', function() {
        it('should use default interval when trying to use interval zero', function() {
            var worker = new Timer(0);
            expect(worker.interval).to.be(1000);
        });

        it('should use default interval when trying to use negative interval', function() {
            var worker = new Timer(-1);
            expect(worker.interval).to.be(1000);
        });
    });

    describe('#start', function() {
        it('should emit start event', function(done) {
            instance.on('start', function() {
                done();
            });

            instance.start();
        });

        it('should return true when first time', function() {
            expect(instance.start()).to.be(true);
        });

        it('should return false when sencod time', function() {
            instance.start();
            expect(instance.start()).to.be(false);
        });
    });

    describe('#stop', function() {
        it('should emit end event', function(done) {
            instance.on('end', function() {
                done();
            });

            instance.start();
            instance.stop();
        });

        it('should not be started', function() {
            instance.start();
            instance.stop();
            expect(instance.started()).to.be(false);
        });

        it('should allow a new start', function() {
            instance.stop();
            expect(instance.start()).to.be(true);
        });
    });
});
