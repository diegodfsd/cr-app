'use strict';

var expect = require('expect.js'),
    promise = require('bluebird');

describe('Models/User', function() {
    var User;

    beforeEach(function () {
        User = require('../../../server/models').User;
    });

    describe('create', function() {

        it('should create successfully when using all required fields', function() {
            return User.create({name: 'John Doe', balance: 1000 }).then(function(user) {
                expect(user.name).to.equal('John Doe');
                expect(user.balance).to.equal(1000);
            });
        });

        it('should not create successfully without name', function() {
            return User.create({balance: 1000 }).catch(function(err) {
                expect(err).not.to.be(null);
                expect(err.message).to.contain('name cannot be null');
            });
        });

        it('should not create successfully without balance', function() {
            return User.create({name: 'John Doe'}).catch(function(err) {
                expect(err).not.to.be(null);
                expect(err.message).to.contain('balance cannot be null');
            });
        });
    });

    describe('instance methods', function() {
        describe('#debit', function() {
            describe('with success', function() {
                it('should be decrease balance', function() {
                    return User.create({name: 'John Doe', balance: 1000 }).then(function(user) {
                        user.debit(500);

                        return user.save().then(function(user) {
                            expect(user.balance).to.be(500);
                        });
                    });
                });
            });

            describe('with error', function() {
                it('should raised error when the balance is insuficient', function() {
                    return User.create({name: 'John Doe', balance: 1000 }).then(function(user) {
                        var fn = function() { user.debit(1001); };

                        expect(fn).to.throwException(/Insufficient founds./);
                    });
                });
            });
        });

        describe('#credit', function() {
            it('should be increase balance', function() {
                return User.create({name: 'John Doe', balance: 1000 }).then(function(user) {
                    user.credit(500);

                    return user.save().then(function(user) {
                        expect(user.balance).to.be(1500);
                    });
                });
            });
        });
    });

    describe('associate an item', function() {
        it('should create successfully with the required fields', function() {
            var Item = require('../../../server/models').Item;

            return User.create({name: 'John Doe', balance: 1000 }).then(function(user) {
                user.createItem({ name: 'Stone', image: 'stone.png' }, { amount: 10 }).then(function(item) {
                    expect(item).not.to.be(null);
                    expect(item.name).to.be('Stone');
                    expect(item.image).to.be('stone.png');
                });
            });
        });
    });
});
