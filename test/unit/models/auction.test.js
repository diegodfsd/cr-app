'use strict';

var expect = require('expect.js'),
    promise = require('bluebird');

describe('Models/Auction', function() {
    var Auction;

    beforeEach(function () {
        Auction = require('../../../server/models').Auction;
    });

    describe('create', function() {

        it('should create successfully when using all required fields', function() {
            var Item = require('../../../server/models').Item;
            var User = require('../../../server/models').User;

            return promise.all([
                User.create({ name: 'John Doe', balance: 1000 }),
                Item.create({ name: 'Stone', image: 'stone.png' })
            ]).spread(function(user, item) {

                return Auction.create({ quantity: 10, minimunBid: 50.5, duration: 90, userId: user.id, itemId: item.id }).then(function(auction) {
                    expect(auction.quantity).to.be(10);
                    expect(auction.minimunBid).to.be(50.5);
                    expect(auction.duration).to.be(90);
                    expect(auction.status).to.be('pending');
                    expect(auction.winningBid).to.be(null);
                    expect(auction.startedAt).to.be(null);
                    expect(auction.completedAt).to.be(null);
                });
            });
        });

        it('should not create successfully without quantity', function() {
            return Auction.create({ minimunBid: 50.5, duration: 90, userId: 1, itemId: 1 }).catch(function(err) {
                expect(err).not.to.be(null);
                expect(err.message).to.contain('quantity cannot be null');
            });
        });

        it('should not create successfully without minimunbid', function() {
            return Auction.create({ quantity: 15, duration: 90, userId: 1, itemId: 1 }).catch(function(err) {
                expect(err).not.to.be(null);
                expect(err.message).to.contain('minimunBid cannot be null');
            });
        });

        it('should not create successfully without duration', function() {
            return Auction.create({ quantity: 15, minimunBid: 100.0, userId: 1, itemId: 1 }).catch(function(err) {
                expect(err).not.to.be(null);
                expect(err.message).to.contain('duration cannot be null');
            });
        });

        it('should not create successfully without userId', function() {
            return Auction.create({ quantity: 15, minimunBid: 100.0, duration: 90, itemId: 1 }).catch(function(err) {
                expect(err).not.to.be(null);
                expect(err.message).to.contain('null value in column "userId"');
            });
        });

        it('should not create successfully without itemId', function() {
            return Auction.create({ quantity: 15, minimunBid: 100.0, duration: 90, userId: 1 }).catch(function(err) {
                expect(err).not.to.be(null);
                expect(err.message).to.contain('null value in column "itemId"');
            });
        });
    });

    describe('instance methods', function() {
        describe('#placeBid', function() {
            it('should create successfully when using all required fields', function() {
                var Item = require('../../../server/models').Item;
                var User = require('../../../server/models').User;
                var Bid = require('../../../server/models').Bid;

                return promise.all([
                    User.create({ name: 'John Doe', balance: 1000 }),
                    Item.create({ name: 'Stone', image: 'stone.png' })
                ]).spread(function(user, item) {

                    return Auction.create({ quantity: 10, minimunBid: 50.5, duration: 90, userId: user.id, itemId: item.id })
                    .then(function(auction) {
                        return auction.placeBid({ value: 55.9, userId: user.id }).then(function(bid) {
                            expect(bid.value).to.be(55.9);
                            expect(bid.userId).to.be(user.id);
                            expect(bid.auctionId).to.be(auction.id);
                        });
                    });
                });
            });
        });
    });
});
