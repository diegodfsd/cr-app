'use strict';

var expect = require('expect.js');

describe('Models/index', function () {
    var models;

    before(function(){
        models = require('../../../server/models');
    });

    it('returns the user model', function () {
        expect(models.User).to.be.ok();
    });

    it('returns the item model', function () {
        expect(models.Item).to.be.ok();
    });

    it('returns the bid model', function () {
        expect(models.Bid).to.be.ok();
    });

    it('returns the auction model', function () {
        expect(models.Auction).to.be.ok();
    });

    it('returns the inventory model', function () {
        expect(models.Inventory).to.be.ok();
    });
});
