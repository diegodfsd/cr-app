'use strict';

var expect = require('expect.js'),
    promise = require('bluebird');

describe('Models/Bid', function() {
    var Bid;

    beforeEach(function () {
        Bid = require('../../../server/models').Bid;
    });

    describe('create', function() {

        it('should create successfully when using all required fields', function() {
            var Auction = require('../../../server/models').Auction;
            var User = require('../../../server/models').User;
            var Item = require('../../../server/models').Item;

            return promise.all([
                User.create({ name: 'John Doe', balance: 1000 }),
                Item.create({ name: 'Stone', image: 'stone.png' })
            ]).spread(function(user, item) {

                return Auction.create({ quantity: 10, minimunBid: 50.5, duration: 90, userId: user.id, itemId: item.id }).then(function(auction) {

                    return Bid.create({value: 100.0, userId: user.id, auctionId: auction.id }).then(function(bid) {
                        expect(bid.value).to.be(100.0);
                        expect(bid.userId).to.be(user.id);
                        expect(bid.auctionId).to.be(auction.id);
                    });
                });
            });
        });

        it('should not create successfully without value', function() {
            return Bid.create({ userId: 1, auctionId: 1 }).catch(function(err) {
                expect(err).not.to.be(null);
                expect(err.message).to.contain('value cannot be null');
            });
        });

        it('should not create successfully without userId', function() {
            return Bid.create({ value: 100.1, auctionId: 1 }).catch(function(err) {
                expect(err).not.to.be(null);
                expect(err.message).to.contain('null value in column "userId"');
            });
        });

        it('should not create successfully without auctionId', function() {
            return Bid.create({ value: 100.1, userId: 1 }).catch(function(err) {
                expect(err).not.to.be(null);
                expect(err.message).to.contain('null value in column "auctionId"');
            });
        });
    });
});
