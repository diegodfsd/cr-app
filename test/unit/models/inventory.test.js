'use strict';

var expect = require('expect.js'),
    promise = require('bluebird');

describe('Models/Inventory', function() {
    var Inventory;

    beforeEach(function () {
        Inventory = require('../../../server/models').Inventory;
    });

    describe('create', function() {

        it('should create successfully when using all required fields', function() {

            var Item = require('../../../server/models').Item;
            var User = require('../../../server/models').User;

            return promise.all([
                User.create({ name: 'John Doe', balance: 1000 }),
                Item.create({ name: 'Stone', image: 'stone.png' })
            ]).spread(function(user, item) {

                return Inventory.create({ amount: 100, userId: user.id, itemId: item.id }).then(function(inventory) {
                    expect(inventory.amount).to.be(100);
                    expect(inventory.userId).to.be(user.id);
                    expect(inventory.itemId).to.be(item.id);
                });
            });
        });

        it('should not create successfully without amount', function() {
            return Inventory.create({ userId: 1, itemId: 1 }).catch(function(err) {
                expect(err).not.to.be(null);
                expect(err.message).to.contain('amount cannot be null');
            });
        });

        it('should not create successfully without userId', function() {
            return Inventory.create({ amount: 50, itemId: 1 }).catch(function(err) {
                expect(err).not.to.be(null);
                expect(err.message).to.contain('null value in column "userId"');
            });
        });

        it('should not create successfully without itemId', function() {
            return Inventory.create({ amount: 15, userId: 1 }).catch(function(err) {
                expect(err).not.to.be(null);
                expect(err.message).to.contain('null value in column "itemId"');
            });
        });
    });
});
