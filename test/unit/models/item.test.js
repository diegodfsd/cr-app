'use strict';

var expect = require('expect.js');

describe('Models/Item', function() {
    var Item;

    beforeEach(function () {
        Item = require('../../../server/models').Item;
    });

    describe('create', function() {

        it('should create successfully when using all required fields', function() {
            return Item.create({name: 'Gold', image: 'gold.png' }).then(function(item) {
                expect(item.name).to.equal('Gold');
                expect(item.image).to.equal('gold.png');
            });
        });

        it('should not create successfully without name', function() {
            return Item.create({ image: 'wood.png' }).catch(function(err) {
                expect(err).not.to.be(null);
                expect(err.message).to.contain('name cannot be null');
            });
        });

        it('should not create successfully without image', function() {
            return Item.create({name: 'Wood'}).catch(function(err) {
                expect(err).not.to.be(null);
                expect(err.message).to.contain('image cannot be null');
            });
        });
    });
});
